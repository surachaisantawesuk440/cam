package strct

import (
	"encoding/json"
	"fmt"

	"google.golang.org/protobuf/types/known/structpb"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service/collection/workload/openstack"
)

// ToAuthOptions converts the protobuf value to Openstack AuthOptions
func ToAuthOptions(v *structpb.Value) (authOpts *openstack.AuthOptions, err error) {
	// Get openstack auth opts from configuration
	value := v.GetStructValue().AsMap()

	if value == nil || len(value) == 0 {
		err = fmt.Errorf("converting raw configuration to map is empty or nil")
		return
	}

	// First, we have to marshal the configuration map
	jsonbody, err := json.Marshal(value)
	if err != nil {
		err = fmt.Errorf("could not marshal configuraton")
		return
	}

	// Then, we can store it back to the gophercloud.AuthOptions
	if err = json.Unmarshal(jsonbody, &authOpts); err != nil {
		err = fmt.Errorf("could not parse configuration: %w", err)
		return
	}

	return
}
