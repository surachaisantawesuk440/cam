package authsec

import "clouditor.io/clouditor/voc"

// Value represents the Value of an evidence in the case of the Authentication Security CM
type Value struct {
	// Clouditor's Resource properties ID and Types have to be set that Evaluation will not fail
	voc.Resource

	// OAuthGrantTypes metric properties
	*OAuthGrantTypes `json:"oAuth,omitempty"`
	// APIOAuthProtected metric properties
	*APIOAuthProtected `json:"apiOAuthProtected,omitempty"`
}

type OAuthGrantTypes struct {
	GrantTypes                                         []string `json:"grantTypes"`
	IDTokenSigningAlgValuesSupported                   []string `json:"idTokenSigningAlgValuesSupported"`
	UserinfoSigningAlgValuesSupported                  []string `json:"userinfoSigningAlgValuesSupported"`
	RequestObjectSigningAlgValuesSupported             []string `json:"requestObjectSigningAlgValuesSupported"`
	TokenEndpointAuthSigningAlgValuesSupported         []string `json:"tokenEndpointAuthSigningAlgValuesSupported"`
	RevocationEndpointAuthSigningAlgValuesSupported    []string `json:"revocationEndpointAuthSigningAlgValuesSupported"`
	IntrospectionEndpointAuthSigningAlgValuesSupported []string `json:"introspectionEndpointAuthSigningAlgValuesSupported"`
	IDTokenEncryptionAlgValuesSupported                []string `json:"idTokenEncryptionAlgValuesSupported"`
	IDTokenEncryptionEncValuesSupported                []string `json:"idTokenEncryptionEncValuesSupported"`
	UserinfoEncryptionAlgValuesSupported               []string `json:"userinfoEncryptionAlgValuesSupported"`
	UserinfoEncryptionEncValuesSupported               []string `json:"userinfoEncryptionEncValuesSupported"`
	RequestObjectEncryptionAlgValuesSupported          []string `json:"requestObjectEncryptionAlgValuesSupported"`
	RequestObjectEncryptionEncValuesSupported          []string `json:"requestObjectEncryptionEncValuesSupported"`
}

type APIOAuthProtected struct {
	Url    string `json:"url"`
	Status string `json:"status"`
}
