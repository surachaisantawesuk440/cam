image: golang:1.19

variables:
  REPO_NAME: gitlab.com/gaia-x/data-infrastructure-federation-services/cam
  NAMESPACE: "cam-${CI_COMMIT_REF_SLUG}"

default:
  timeout: 15m

stages:
  - build
  - build-images
  - deploy
  - postdeploy

#include: "cmd/*/.gitlab-ci.yml"

.buildimagegeneric:
  #tags: [ privatecluster ]  #uncomment this if you want to build on dev4 cluster
  variables:
    DOCKERFILE: "Dockerfile"
    DESTINATION: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}:${CI_COMMIT_REF_SLUG}"
    #REGISTRY_PROXY_REMOTEURL: https://registry-1.docker.io
    REGISTRY_PROXY_REMOTEURL: https://mirror.gcr.io
  services:
    - name: registry:2
      alias: registrymirror.local
  retry: 1
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "$DOCKERFILE ==> {$DESTINATION}"
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile  "${DOCKERFILE}"
      --destination "${DESTINATION}"
      --registry-mirror registrymirror.local:5000
#      --registry-mirror mirror-docker-registry.gitlab.svc.cluster.local:5000
#      --cache-repo      kanikocache-docker-registry.gitlab.svc.cluster.local:5000/yea/whatever        #destination is used as a cache repo if none specified
#      --target NAMEOFTHETARGET

.buildimage:
  variables:
    MICROSERVICE: "${CI_JOB_NAME}"
    DOCKERFILE: "${CI_PROJECT_DIR}/cmd/${MICROSERVICE}/Dockerfile"
    DESTINATION: "${CI_REGISTRY_IMAGE}/${MICROSERVICE}:${CI_COMMIT_REF_SLUG}"
  extends: .buildimagegeneric

.go-cache:
  variables:
    GOPATH: $CI_PROJECT_DIR/.go
  before_script:
    - mkdir -p .go
  cache:
    paths:
      - .go/pkg/mod/

dimages:
  extends: .buildimage
  stage: build-images
  needs: [go-build-and-test]
  dependencies: [go-build-and-test]
  rules:
    - if: $CI_COMMIT_REF_NAME == "main" || $CI_PIPELINE_SOURCE == "merge_request_event"
  parallel:
    matrix:
      - MICROSERVICE:
          [
            cam-api-gateway,
            cam-collection-authsec,
            cam-collection-commsec,
            cam-collection-integrity,
            cam-collection-workload,
            cam-eval-manager,
            cam-req-manager,
          ]

go-build-and-test:
  rules:
    - if: $CI_COMMIT_REF_NAME == "main" || $CI_PIPELINE_SOURCE == "merge_request_event"
  extends: .go-cache
  stage: build
  image: golang:1.19
  script:
    - apt update && apt install -y nodejs npm
    - pushd dashboard
    - npm install && npm run build
    - popd
    - go build cmd/cam-api-gateway/cam-api-gateway.go
    - go build cmd/cam-collection-authsec/cam-collection-authsec.go
    - go build cmd/cam-collection-integrity/cam-collection-integrity.go
    - go build cmd/cam-collection-workload/cam-collection-workload.go
    - go build cmd/cam-eval-manager/cam-eval-manager.go
    - go build cmd/cam-req-manager/cam-req-manager.go
    - go install github.com/jstemmer/go-junit-report github.com/boumenot/gocover-cobertura
    - go test -v -coverprofile=coverage.txt -covermode=atomic 2>&1 ./... | tee gotest.results.txt
    - cat gotest.results.txt | .go/bin/go-junit-report -set-exit-code > junit.xml
    - .go/bin/gocover-cobertura < coverage.txt > coverage.xml
  artifacts:
    when: always
    paths:
      - gotest.results.txt
      - junit.xml
      - coverage.xml
      - cam-req-manager
      - cam-eval-manager
      - cam-api-gateway
      - cam-collection-authsec
      - cam-collection-integrity
      - cam-collection-workload
    reports:
      junit: junit.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

deploy:
  tags: [privatecluster] #uncomment this if you want to build on dev4 cluster
  stage: deploy
  needs: [dimages]
  image: alpine/k8s:1.22.9
  script:
    - kubectl config use-context gaia-x/data-infrastructure-federation-services/cam:dev4
    - helm upgrade --install -n ${NAMESPACE} --create-namespace ${CI_COMMIT_REF_SLUG} ${CI_PROJECT_DIR}/helm --set tag="${CI_COMMIT_REF_SLUG}" --set CI_COMMIT_REF_SLUG="${CI_COMMIT_REF_SLUG}" --set CI_COMMIT_SHA="${CI_COMMIT_SHA}"
      --set services.reqManager.oauth2.clientSecret="${CAM_REQ_MANAGER_OAUTH2_CLIENT_SECRET}"
      --set services.evalManager.oauth2.clientSecret="${CAM_EVAL_MANAGER_OAUTH2_CLIENT_SECRET}"
      --set services.collectionAuthSec.oauth2.clientSecret="${CAM_COLLECTION_AUTHSEC_OAUTH2_CLIENT_SECRET}"
      --set services.collectionCommSec.oauth2.clientSecret="${CAM_COLLECTION_COMMSEC_OAUTH2_CLIENT_SECRET}"
      --set services.collectionIntegrity.oauth2.clientSecret="${CAM_COLLECTION_INTEGRITY_OAUTH2_CLIENT_SECRET}"
      --set services.collectionWorkload.oauth2.clientSecret="${CAM_COLLECTION_WORKLOAD_OAUTH2_CLIENT_SECRET}"
      #--wait
    - kubectl get secrets -n ingress-nginx wildcard-gxfs-dev -o yaml | grep -v 'namespace:\|resourceVersion:\|uid:\|creationTimestamp' | kubectl apply -f - -n "${NAMESPACE}"
    - kubectl get secrets -n gxfs-fraunhofer postgres.fraunhofer-database1-postgres.credentials.postgresql.acid.zalan.do -o yaml | grep -v 'namespace:\|resourceVersion:\|uid:\|creationTimestamp' | kubectl apply -f - -n "${NAMESPACE}"
    - kubectl apply -n cam-main -f ${CI_PROJECT_DIR}/manifests/cam-ingress-default.k8s.yaml
  rules:
    - if: $CI_COMMIT_REF_NAME == "main" || $CI_PIPELINE_SOURCE == "merge_request_event"

deploytest:
  parallel:
    matrix:
      - ADDRESS:
          [
            "cam-req-manager.${NAMESPACE}:50100",
            "cam-eval-manager.${NAMESPACE}:50101",
            "cam-collection-commsec.${NAMESPACE}:50051",
            "cam-collection-authsec.${NAMESPACE}:50052",
            "cam-collection-integrity.${NAMESPACE}:50053",
            "cam-collection-workload.${NAMESPACE}:50054",
          ]
  tags: [privatecluster] #uncomment this if you want to build on dev4 cluster
  stage: deploy
  needs: [deploy]
  image: docker.io/namely/grpc-cli
  script:
    - grpc_cli ls "${ADDRESS}" -l
  allow_failure: true #for now
  rules:
    - if: $CI_COMMIT_REF_NAME == "main" || $CI_PIPELINE_SOURCE == "merge_request_event"

deploytestgateway:
  tags: [privatecluster] #uncomment this if you want to build on dev4 cluster
  stage: deploy
  needs: [deploy]
  image: alpine/k8s:1.22.9
  script:
    - "${CI_PROJECT_DIR}/tests/smoketests_gateway.sh ${NAMESPACE}"
  rules:
    - if: $CI_COMMIT_REF_NAME == "main" || $CI_PIPELINE_SOURCE == "merge_request_event"

deploydelete:
  tags: [privatecluster] #uncomment this if you want to build on dev4 cluster
  variables:
    GIT_STRATEGY: none
  stage: postdeploy
  image: alpine/k8s:1.22.9
  script:
    - kubectl config use-context gaia-x/data-infrastructure-federation-services/cam:dev4
    - helm uninstall -n ${NAMESPACE} ${CI_COMMIT_REF_SLUG}
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE != "merge_request_event"
      when: manual
      allow_failure: true
