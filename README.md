# Continuous Automated Monitoring

[![](https://godoc.org/gitlab.com/gaia-x/data-infrastructure-federation-services/cam?status.svg)](https://pkg.go.dev/gitlab.com/gaia-x/data-infrastructure-federation-services/cam)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/gaia-x/data-infrastructure-federation-services/cam)](https://goreportcard.com/report/gitlab.com/gaia-x/data-infrastructure-federation-services/cam)

This project contains the reference implementation of the Gaia-X Continuous Automated Monitoring component.

# Installation
Assuming that you have access to an kubernetes cluster, are in the right kubernetes context, and have helm installed:
```
git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/cam.git
helm upgrade --install your_name_here cam/helm
```


## Clarifications of the Specification

This implementation follows closely the specification of the [Continuous Automated Monitoring](https://www.gxfs.eu/download/1731/) component of the Gaia-X Federated Services. However, there are minor clarifications to the specification that we chose because the specification was ambiguous or contradictory.

### General

* Usage of `String` instead of `Int` for all ID fields: All ID fields are now UUIDs and their internal protobuf representation is a string. This was necessary for two reasons: First, some ID fields need to be compatible with OSCAL (such as the control ID) and their identifiers follow a string-based approach. Second, some identifiers, such as as the evidence ID need to be uniquely generated on their own by the collection modules, which makes it ideally suited for a UUID. This change is purely internal, as the change of representation to the outside world via the REST API is transparent.
* The specification did not include a path layout for the REST API. A sensible, future-compatible approach using a numbered schema, such as `/v1/configuration` has been chosen.

### Interface Modifications

* The specification was missing function calls to add/edit/remove target cloud services. Appropriate RPC calls, such as `RegisterCloudService` have been added to the `Configuration` service interface and exposed via the REST API.
* The `Configuration` interface also lacked a possibility to configure the needed `ServiceConfiguration` for a particular cloud service. This functionality has been added via the `ConfigureCloudService` function call.
* A `ListControls` call has been added to the `Configuration` interface, that returns a list of all relevant controls configured in the CAM.
* Function calls to list and retrieve a particular evidence from the evaluation manager have been added to the `Evaluation` service interface in the form of `ListEvidence` and `GetEvidence` and exposed via the REST API.
* Added `CalculateComplianceRequest` to the `Evaluation` service interface to trigger compliance calculation from the requirement manager.
* The field `metric_id` has been removed from the `StartCollectingRequest` since collection modules do not have a strong coupling to metrics any more.

* The RPC call `FindCollectionModule` has been removed, since collection modules do not have a strong coupling to metrics anymore.

### Object Modifications

* An `Error` object class has been introduced and added as a property `error` to the `Evidence` class. The reasoning behind this, is that during the evidence collection an error might occur that leads to the generation of a corrupt / invalid evidence. Instead of silently ignoring errors, these can now be sent to the evaluation manager for further processing.
* The field `gathered_using` in `Evidence` has been removed. A collection module is not tied directly to one metric and since the collection modules creates the evidence, this field has no meaning.
* The field `metric` of `CollectionModule` has been extended to `metrics` and specifies a list of metrics that the collection module feels responsible for. In the future, however we want to completely de-couple metrics and collection modules, so this field might get removed again.
* * The `raw_configuration` field in `ServiceConfiguration` is now specified as a `google.protobuf.Any` field, which allows embedded any protobuf message that represents the service configuration needed for the collection module. 
* The `CollectionModule` object now has field `config_message_type_url` which corresponds to the type URL of the `raw_configuration` that the collection module expects in its `StartCollectingRequest`.
